## Simulation Inflorescences

### Édition Conteneur Mutek 2024

![Sabrina_Ratté_simulation_20240627_1](./Sabrina_Ratté_simulation_20240627_1.jpg)

![Sabrina_Ratté_simulation_20240627_2](./Sabrina_Ratté_simulation_20240627_2.jpg)

![Sabrina_Ratté_simulation_20240627_3](./Sabrina_Ratté_simulation_20240627_3.jpg)

![Sabrina_Ratté_simulation_20240627_4](./Sabrina_Ratté_simulation_20240627_4.jpg)


#### Container  
* Voir [référence](/annexes/container/readme.md) 

#### Murs 
* 3 murs recouvert de papier iridescent sur support rigide
    * Le long mur du fond 
        * Adéquatement soufflé pour permettre le raccordement électrique et sonore (Écran, haut-parleurs) 
    * Les deux murs latéraux

#### Plancher
* Le plancher recouvert de pelicule holographique sur support rigide 
* Déposé et maintenue sécuritairement 
* Pas de public sur le plancher holographique

#### Écran HDMI

* Celle dans la simulation fait 80" de diagonale
* Le plus grand le mieux
* Signal 1080p/30fps 
* Boucle vidéo (15 minutes)  
* Seamless 

#### Haut-parleurs

* Support à haut-parleurs doté d'angles ajustables 
    * Distance du mur
    * Gauche/Droite  
    * Haut/Bas  

* Intégration le plus invisible possible sans toutefois compromettre la qualité sonore

#### Lumière Spot

* Éclairages en douche type spot ~18°
* Fixation magnétique 
* Cercle froid sur chacune des sculptures 
* [Fiche technique lx](/annexes/lx_spot/)


[techrider](../techrider/README.md ":include")
