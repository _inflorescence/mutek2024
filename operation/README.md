# Manuel d'opération

## Précautions 

* Évitez de marcher dans le conteneur
* Assurez-vous qu'aucun public ne marche dans le conteneur 

## Ouverture

* Ouvrir le conteneur.
* Allumer la télévision via la télécommande.
* Vérifier le fonctionnement général de l'installation.
* Replacer la télécommande dans l'espace créé par la porte secondaire.
* L'œuvre d'art est visible via la porte principale.

## Fermeture

* Éteindre la télévision.
* Fermer et verrouiller la porte principale.
* S'assurer que la télécommande est accessible dans l'espace prévu à cet effet.
* Fermer et verrouiller la porte secondaire.



## Opérations

### Vidéo

#### Écran principal
> [!TIP] 
> Assurez-vous que la vidéo joue en boucle 

> [!WARNING] 
> Si la vidéo ne joue pas, utilisez la télécommande pour  allumer la télévision et sélectionner le média à lire en boucle


#### Écrans sculptures

> [!TIP] 
> Assurez-vous que toutes les sculptures jouent leur vidéo respective en boucle 

> [!WARNING] 
> Si une vidéo ne joue pas, débrancher/rebrancher le commutateur Ethernet POE depuis la barre de surtension située dans l'espace technique de la porte secondaire. Toutes les sculptures seront redémarrées dans leur état de lecture.

> [!CAUTION]
> Si le problème persiste, communiquez avec Guillaume.



### Audio

#### Haut-parleurs principaux

> [!TIP] 
> Assurez-vous que le son est diffusé par les deux enceintes installées dans les coins du conteneur au volume calibré par l'artiste

> [!WARNING] 
> Si le son ne provient pas des enceintes principales, utilisez la télécommande pour régler le volume sonore au niveau préréglé

#### Haut-parleurs sculptures 

> [!TIP] 
> Assurez-vous que le son est diffusé par tous les mini haut-parleurs installés sur les sculptures.

> [!WARNING] 
> Si le son ne provient pas des mini haut-parleurs, assurez-vous qu'ils sont allumés et ajustez le volume au maximum.

> [!WARNING] 
> Si le son ne provient toujours pas des mini haut-parleurs, inspectez la connexion entre les haut-parleurs et le micro-ordinateur.

> [!CAUTION]
> Si le son ne fonctionne toujours pas, redémarrez le micro-ordinateur en débranchant le câble Ethernet, en attendant 5 secondes, puis en le rebranchant. Le temps de démarrage ne doit pas dépasser 3 minutes.

> [!CAUTION]
> Si le son ne fonctionne toujours pas, communiquez avec Guillaume.

### Lumières 

#### Éclairage du plafond

> [!TIP] 
> Assurez-vous que les spots de lumières sont allumés sur les œuvres, il se peut que ce soit presque imperceptible, surtout de jour. C'est normal.  

> [!WARNING] 
> Si une ou des spots sont éteints, vérifier les connexions dans l'espace technique via la porte secondaire. 



#### Sculptures LED lights and Screens

> [!TIP] 
> Assurez-vous que toutes les lumières LED sont allumées et sont animées

> [!WARNING] 
> Si une vidéo ne joue pas, débrancher/rebrancher le commutateur Ethernet POE depuis la barre de surtension située dans l'espace technique de la porte secondaire. Toutes les sculptures seront redémarrées dans leur état de lecture.

> [!CAUTION]
> Si le problème persiste, communiquez avec Guillaume.



### Matériel iridescent 

#### Cleaning 

Un tissu doux devrait suffire. De l'eau déminéralisée peut être utilisée légèrement pour les taches tenaces, mais doit être testée au préalable sur une petite partie du matériau.
Ne marchez pas sur le sol iridescent, utilisez un bâton long. Si vous devez absolument marcher dessus, pour vérifier si les haut-parleurs sont allumés par exemple, veuillez enlever vos chaussures.
