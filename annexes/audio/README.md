## Audio 

### Speakers 

2x Active speaker, provided by the venue 

#### Meyer UPM-1P

![Meyer UPM-1P](Meyer_UPM-1P.png)

[Meyer UPM-1P DataSheet](/annexes/audio/upm-ds_d3.pdf)

[Meyer UPM-1P Manual](/annexes/audio/upm-oi_d2.pdf)


### Audio Distribution

#### Analog output with 1/8" to XLR adapter

```mermaid
graph TD
media[Media Loop MP4 Stereo] --> Screen[Video display Screen]
Screen --Headphone output--> adapter[1/8'' stereo to 2x XLR M] 
adapter --XLR--> SpeakerL
adapter --XLR--> SpeakerR
```
##### 1/8'' stereo to 2x XLR M

![1/8'' stereo to 2x XLR M](passive_headphoneToXLR.png)


#### Analog output with passive DI

If solution above is too noisy and/or quiet 

```mermaid
graph TD
media[Media Loop MP4 Stereo] --> Screen[Video display Screen]
Screen --Headphone output--> adapter[1/8'' stereo to 2x 1/4'' or stereo 3.5mm ] -->DI[Passive Stereo Input box]
DI --XLR--> SpeakerL
DI --XLR--> SpeakerR
```


### Power

#### PowerCon Daisy Chain
![powercon UPM-1P Daisy Chain](powercon_UPM-1P.png)

```mermaid
graph LR
AC--PowerCon-->SpeakerL--PowerCon LoopOut-->SpeakerR
```

### Rigging 

#### ¿MUB-UPM U-Bracket ?

![MUB-UPM U-Bracket](MUB-UPM_U-Bracket.png)

Allows a UPM-1P loudspeaker to be mounted to a wall (in either vertical or horizontal orientations), to the ceiling or onto the
floor.

#### ¿MYA-UPM Yoke?

![MYA-UPM Yoke ](MYA-UPM_Yoke.png)

Cradle-style mounting yoke that suspends a single UPM-1P loudspeaker and supports a wide range of horizontal and vertical
adjustment. The yoke attaches to the top and bottom nut plates and includes 3/8-inch-16 and M10 hardware.
