## Synoptique

### Branchements

#### Overview
```mermaid
graph TD
%% TECH
%% VIDEO
    is[4x INFLO player sculpture]
    B[Big Screen HDMI MUTEK]

%% LX 
    SPOT[4x Light Beam Spot Light]

%% AUDIO
   B --HDMI audio --> SPK[2x SPEAKERS MUTEK]

%% Network  
    WAN[Secure WIFI Network]
    WAN -.- Router
    Router[Router INFLO]-->Switch[Switch POE]
    Switch --cat6a--> is
    Switch --cat6a--> im

%% AC
    AC ==4x10W==> SPOT
    AC ==15W AC/DC 5V3A==> Router
    AC ==150W IEC ==> Switch
    AC ==¿200W?==> SPK
    AC ==¿800W?==> B
```


#### Sculpture Inflorescence (4x) 

##### Objects

```mermaid
graph TD
Switch[Switch POE+] --CAT6Slim--> PI[pi POE+]
PI --USB--> Speaker[USB Speaker with LINE IN]
PI --1/8-Jack--> Speaker
PI --DSI--> Screen[10.1 inch DSI 1280x800 ]
PI --USB--> MCU[M5Atom] --> LED[100x Flex Fairy Light]
```


#### Network
```mermaid
graph TD
%% NETWORK
    Internet[Secured Private WIFI - QDS]
    R[Router WIFI INFLO]
    S[Switch Ethernet POE+ INFLO]
    Internet -. WAN .-> R
    R --LAN--> S
    S --CAT6Slim--> i1
    S --CAT6Slim--> i2
    S --CAT6Slim--> i3
    S --CAT6Slim--> i4
```

#### Audio

```mermaid
graph TD
%% AUDIO
    BigScreen --> DAC[HDMI 2.0 PCM] --> Speakers[2x Speakers full spectrum]
    Speakers --> Left[Left Channel]
    Speakers --> Right[Right Channel]
```