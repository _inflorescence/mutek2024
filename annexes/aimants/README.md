
### Magnet on/off

Potentiellement pour accrochage équipement AV

#### Oudtinx Magnetic Holder Bed 
* 176lbs / 80 kg
* ~25$
* M8 x1.25 threaded hole 
* size 2.125" high x 2" wide by 3" deep 
* https://www.amazon.ca/dp/B07YJBZYZH/?th=1
* ![Magnet on/off Oudtinx 80 kg ](image-4.png)

#### Accusize Magnetic Base On/OFF 6010-0121 
* 176lbs / 80 kg
* ~30$
* https://www.amazon.ca/dp/B0D3RH18QF?th=1
* ![6010-0121 - tech draw](image.png)

#### Accusize Magnetic Base On/OFF  6010-0141
*  264lbs / 120kg 
* 40$
* https://www.amazon.ca/dp/B09NLDD9TB?th=1
* ![6010-0141 - tech draw](image-1.png)


### Countersunk Neodymium Magnets 


Potentiellement pour accrochage [surface rigide holographique](/annexes/disposition_SRH/)

#### ø 25mm 
* 55 lbs / 25 kg
* 1.05 $ / unit
* ![Countersunk Neodymium Magnets ø25mm](image-2.png)


####  ø 36 mm
* 150lb / 65 kg
* 3.10$ / unit
* ![Countersunk Neodymium Magnets ø36mm](image-3.png)
