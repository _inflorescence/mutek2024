### Tech specs

* AC 120v
* ~10w
* Dimmable 
* no-flicker
* Montable au plafond 
* Lentille avec zoom
* Couleur froide disponible (5000k)
* Plug and play 
  * Preset défini à l'initialisation


#### Branchement

##### Via longeur sur mesure 

1 dimmer

```mermaid
graph TD
    120V --> dimmer  -->  DistributionLX

    DistributionLX --> LX1
    DistributionLX --> LX2
    DistributionLX --> LX3
    DistributionLX --> LX4

```    

##### Via extention
```mermaid
graph TD
    A[120V Circuit] --> B[Extension Cable 1]
    B --> C[Light 1]
    B --> D[Extension Cable 2]
    D --> E[Light 2]
    D --> F[Extension Cable 3]
    F --> G[Light 3]
    F --> H[Extension Cable 4]
    H --> I[Light 4]

```

### Base
#### Magnetique 



### Recommendation/évaluation
#### VANoopee 3 Color Zoomable LED 

Up Lights Indoor Spot Light with Timer, 10W Dimmable Uplighting Indoor Spotlight Accent Lighting for Plant Art, Floor Spotlight Lamp with 5.9FT Plug Cord - Black 2 Pack 

* https://www.amazon.ca/gp/product/B0CPF2T21P/

![alt text](image.png)
![alt text](image-1.png)
![alt text](image-2.png)
![alt text](image-3.png)
![alt text](image-4.png)

<!-- 

#### Cables


* https://www.amazon.ca/dp/B08KSKLFC1/
+ Magnets



![alt text](image-5.png)

![alt text](image-6.png) -->