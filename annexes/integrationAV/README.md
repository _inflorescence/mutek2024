### Équipements AV

#### Branchement

```mermaid
graph TD
SpeakerLeft[Haut-parleur gauche] 
Écran[Écran Vidéo]
SpeakerRight[Haut-parleur droit]


AC ==120v==> SpeakerLeft 
AC ==120v==> Écran
AC ==120v==> SpeakerRight 

Écran --Audio Left --> SpeakerLeft
Écran --Audio Right --> SpeakerRight
media[Média USB] --> Écran
```


#### Grand écran vidéo haute résolution 

##### Lecteur multimédia intégré à l'écran

* Lecture du média en boucle depuis clef USB
* Boucle seamless (pas d'apparence de menu ou autres distractions visuelles)
* Son synchrone à la vidéo depuis le média 
* Communiquer avec l'artiste si la lecture en boucle n'est pas belle (seamless) 

##### Télécommande 

* Controle le son dans les haut-parleur
* Allumer/éteindre la télévision -> allumer éteindre l'installation   

##### Sortie 

#### Haut-Parleurs

##### Actif 

##### Support articulé positionable 