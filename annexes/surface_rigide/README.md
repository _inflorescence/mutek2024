### Spécification de la surface rigide 

* Support rigide pour application de [pellicule holographique autocollante](/annexes/pellicule_holographique/)
* Disponible en format 4' x 8'
* Surface 100% lisse 
* Appliqué, coupé et installé par la production 

#### Dimensions

![4' x 8' surface rigide](./surface_rigide.svg)

#### Matériel ayant déjà fonctionné
* MDF 1/2"

