
#### Corrugation 

[corrugation specification source](https://www.discovercontainers.com/shipping-container-components-classifications/)

##### Container Side Corrugation

![Container_Side_Corrugation](Container_Side_Corrugation.png)

##### Container End Corrugation

![Container_End_Corrugation](Container_End_Corrugation.png)

##### Container door corrugation

![Container door corrugation](Container_door_corrugation.png)


##### Container Roof Corrugation

![Container Roof Corrugation](Container_Roof_Corrugation.png)


> source https://www.discovercontainers.com/shipping-container-components-classifications/