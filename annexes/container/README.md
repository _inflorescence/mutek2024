## Container

### Specifications

#### 20′ Open Side, side opening container 50/50

##### Container Front open
![20′ Open Side, side opening container front](./20_Open_Side_side_opening_container_front.png)

##### Container Front close
![Container Front close](./20_Open_Side_side_opening_container_front_close.png)

##### Container side open
![20′ Open Side, side opening container side](./20_Open_Side_side_opening_container_side_open.png)

##### Container side open, front closed
![20′ Open Side, side opening container side](./20_Open_Side_side_opening_container_side_closed.png)

> Source : https://mccontainers.com/product/20ft-open-side-full-side-access-shipping-container/


##### Container Side open render

| ![render_side_open_containner_front](./render_side_open_containner_front.png) | ![render_side_open_containner_front_perspective.png](./render_side_open_containner_front_perspective.png) |
| - | - |

> Source https://www.mobilemodularcontainers.com/blog/open-sided-shipping-containers


### Standard references 

#### Tech Draw

[20ft_Standard_Side_Opener_5050 technical drawing](/annexes/container/20ft_Standard_Side_Opener_5050.pdf)

#### Specification

20 ft side opening shipping container.  

| Specification | Measurement |
| - | - |
| External Length       | 6.06 m (20ft)         |
| External Width        | 2.438 m (8ft)         |
| External Height       | 2.591 m (8ft 6in)     |
| Internal Length       | 5.898 m (19ft 4in)    | 
| Internal Width	    | 2.34 m (7ft 8in)      |
| Internal Height       | 2.38 m (7ft 9in)      |
| Door Opening Width    | 2.34 m (7ft 8in)      |
| Door Opening Height   | 2.38 m (7ft 9in)      |
| Capacity              | 33.29 m³              |
| Tare Weight           | 2,940 kg              |
| Max Cargo Weight	    | 21,060 kg             |

> Source : https://www.seacontainers.co.nz/shipping-container-dimensions/side-opening-container-dimensions/


#### Corrugation detail

[Corrugation specification](/annexes/container/corrugation.md)