### Branchements


#### AC Distribution 
```mermaid
graph TD
    AC
    AC==>AC1
    AC==>AC2
    AC1[~120VAC φ1]
    AC1 ==15w==> Router
    AC1 ==150w==> SwitchPOE
    AC1 --> LXDimmer
    AC1 ==¿500w?==> BigScreen
    LXDimmer ==10w ==> LX1
    LXDimmer ==10w ==> LX2
    LXDimmer ==10w ==> LX3
    LXDimmer ==10w ==> LX4
    
    AC2[~120VAC φ2]
    AC2 ==¿150w?==> SPKL
    AC2 ==¿150w?==> SPKR
```
