
## Disposition Surface Rigide Holographique (SRH)

#### Volume Container
![Container Simple](./container_simple.svg) 

#### Disposition Panneaux 
![Conteneur inflo](./dispositionSRH.svg)

#### Simulation référence

| ![](../../simulation/Sabrina_Ratté_simulation_20240627_1.jpg)| ![](../../simulation/Sabrina_Ratté_simulation_20240627_2.jpg) |
|-|-|
| ![](../../simulation/Sabrina_Ratté_simulation_20240627_3.jpg) | ![](../../simulation/Sabrina_Ratté_simulation_20240627_4.jpg)




