## LED power Branching

![simulation 3 cropLED](simulation_3_cropLED.png)

### Branchement

```mermaid
graph TD;
    SWITCH[SwitchPOE]
    RPI[Raspberry Pi]
    M5Atom[M5Atom]
    SWITCH --cat6slim--> RPI
    RPI -->|USB| M5Atom[M5Atom HyperSerial]
    M5Atom --> LED0[Fairy Light 0-99]

    LED0 --Signal+GND--> TypeC2Grove[TypeC2Grove]
    SWITCH--cat6slim-->POE5v[POE 48V to 5V]
    POE5v--TypeC -->TypeC2Grove
    TypeC2Grove -->LED1_A[Fairy Light 100+]
    TypeC2Grove -->LED1_B[Fairy Light 100+]
```

### Objets

#### TypeC2Grove
![Unit-TypeC2Grove](Unit-TypeC2Grove.png)

* [M5Stack products/usb-typec2grove-unit](https://shop.m5stack.com/products/usb-typec2grove-unit)

* fournisseur : Digikey 

#### POE5V ->Type C

##### 4amp bulky 

![POE5V ->Type C : 4amp bulky](image-1.png)

[lien amzon](https://www.amazon.ca/dp/B08FLX63WD/)

##### interface network USB POE

![POE5V ->Type C : interface network USB POE](image-2.png)

[lien amazon](https://www.amazon.ca/dp/B0CWP3NZ3G/)

##### Avec un cable de trop

![POE5V ->Type C : Avec un cable de trop](image.png)

* [amazon](https://www.amazon.ca/dp/B0BYSNQQV1/)


### Fixation

#### Magnétique sur porte du container