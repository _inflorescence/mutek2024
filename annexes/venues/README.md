## INFLORESCENCES, 2023

By Sabrina Ratté 
https://sabrinaratte.com/INFLORESCENCES-2023

![Inflorescences 1](./media/inflorescences_thumb.jpg)


Inflorescences unfolds in a hypothetical future, where plants, mushrooms, and unfamiliar critters have undergone mutations to exist in symbiosis with abandoned objects. This project explores the emergence of life forms from what we perceive as inert and forgotten remnants, which continue to evolve and foster new relationships with the ecosystem. Inflorescences portrays a world devoid of humans, yet its evolution is shaped by the remnants they left behind.


Series of 4 videos and 4 sculptures 

A production of the New Now Festival

Multimedia integration: Guillaume Arseneault

Sounds: Roger Tellier Craig




### 4 videos sculptures with sound



|![Inflorescences 1 3:42 minutes ](./media/inflorescences_1.jpg) | ![Inflorescences 2 3:44 minutes ](./media/inflorescences_2.jpg) |
|-|-|
|![Inflorescences 3 3:42 minutes ](./media/inflorescences_3.jpg) | ![Inflorescences 4 3:44 minutes ](./media/inflorescences_4.jpg) |


### Fotografiska, Shanghai, 2024

![Fotografiska, Shanghai, 2024, 1/3](./media/Shanghai_1.jpeg)
![Fotografiska, Shanghai, 2024, 2/3](./media/Shanghai_2.jpeg)
![Fotografiska, Shanghai, 2024, 3/3](./media/Shanghai_3.jpeg)

### Zollverein UNESCO World Heritage Site, New Now Festival, Germany, 2023

![Zollverein UNESCO World Heritage Site, New Now Festival, Germany, 2023 1/3](./media/Zollverein_1.jpeg)
![Zollverein UNESCO World Heritage Site, New Now Festival, Germany, 2023 2/3](./media/Zollverein_3.jpeg)
![Zollverein UNESCO World Heritage Site, New Now Festival, Germany, 2023 3/3](./media/Zollverein_2.jpeg)

### Arsenal Contemporary Arts, Futurs Spéculaire, Montreal, 2023

![ Arsenal Contemporary Arts, Futurs Spéculaire, Montreal, 2023 1/3](./media/arsenal_1.jpeg)
![ Arsenal Contemporary Arts, Futurs Spéculaire, Montreal, 2023 2/3](./media/arsenal_2.jpeg)
![ Arsenal Contemporary Arts, Futurs Spéculaire, Montreal, 2023 3/3](./media/arsenal_3.jpeg)

### Inflorescences sculptures

#### Inflorescences 1 : CRT Television

![Inflorescences 1 : CRT Television](./media/sculpture_1_crt_television.jpeg)

#### Inflorescences 2 : Audio Amplifier

![Inflorescences 2 : Audio Amplifier](./media/sculpture_2_amplifier.jpeg)

#### Inflorescences 3 : Desktop Computer

![Inflorescences 3 : Desktop computer](./media/sculpture_3_computer.jpeg)

#### Inflorescences 4 : VCR

![Inflorescences 4 : VCR](./media/sculpture_4_vcr.jpeg)


