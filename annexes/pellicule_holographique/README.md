## Pellicule holographique autocollante

![Papier autocollant](./image-papier-autocollant.png)


### Dimension 

#### Largeur : 1.27m 
#### Longueur : 50m
#### Épaisseur : 90 microns
#### Poid : 20.5kg


### Lien manufacturier

* [Alibaba](https://www.alibaba.com/product-detail/Factory-Hot-Eco-solvent-Holographic-Laser_1600163619682.html?spm=a2700.details.0.0.7fb72a0bO66ObX)

![tube holographique](image-tube-holographique.png)
