## Tech Rider / Besoins technique 

### Fournis par la production

* Contexte optimale de déploiement

#### Vidéo
* Écran HDMI cablée, alimentée et installée selon la scénographie   
* ~ 80" et plus 
* Système de fixation au container VESA sécuritaire 
* Lecteur multimédia inclus dans l'écran 
    * Lecture vidéo mp4 depuis clef USB  
    * Boucle élégante (seamless)
    * Lecture du média défini à l'initialisation   
* Télécommande de l'écran permettant les réglages suivant
    * Volume dans les haut-parleurs
    * Configuration des paramètres visuels de l'Écran


#### Sonorisation  
* 2 Haut-parleurs (gauche/droit depuis fichier vidéo)
* Système de fixation au container simple, élagante et sécuritaire 
* Position du haut-parleur ajustable
  * haut/bas
  * gauche/droite
  * distance du mur
* Audio Stéréo via Écran HDMI
* Volume via télécomande


#### Électricité    
* 2 circuits AC 120v 
* Branchements électrique et sonore  

#### Scénographie

* Remboursement de la commande de [pélicule autocollante holographique](/annexes/pellicule_holographique/) ~400$
* Manutention, transport, coupe et installation des [surfaces rigides](/annexes/surface_rigide/) en surface lisse et uniforme
* Pose de la  [pélicule autocollante holographique](/annexes/pellicule_holographique/) sur les [surfaces rigides](/annexes/surface_rigide/) adéquatement coupées
* [Disposition de la surface rigide holographique dans le container](/annexes/disposition_SRH/)
* [Intégration élégante du matériel AV](/annexes/integrationAV/) aux panneau rigide holographique
* Passage discret du cablage AV et électrique derriere les surfaces rigide holographique

 
### Fourni par l'artiste

* 4 objets électroniques désuets
* 4 x  écrans 10" avec Raspberry Pi jouant vidéo et lumière LED
* 4 x mini speakers déposés dans les sculptures
* Switch POE
* Router Access point
* 4x Spo
* Calibration artistique


